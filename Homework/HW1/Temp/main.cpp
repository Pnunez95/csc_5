/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 3, 2014, 5:55 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
 
double num_meters;
 
cout << "Enter number of meters." << endl;
cin >> num_meters;
cout << endl;
cout << num_meters << " is ";
cout << num_meters / 1609.344;
cout << " miles, ";
cout << num_meters * 3.281;
cout << " feet, and ";
cout << num_meters * 3.281 * 12;
cout << " inches.";
return 0;
    return 0;
}

