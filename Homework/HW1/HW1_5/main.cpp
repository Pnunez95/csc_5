/*
* Name: Paul Nunez
* Student ID: 2486474
* Date: 2.25.14
* HW:1
* Problem:5
* I certify this is my own work and code
*/

/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on February 26, 2014, 4:26 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

int num1, num2; 
    
    cout << "Enter first integer.\n";
    cin >> num1;
    cout << "Enter second integer.\n";
    cin >> num2;
    cout << "The sum of the first integer and second integer is ";
    cout << num1 + num2;
    cout << " and the product of the first and second integer is ";
    cout << num1 * num2;
    cout << ".\n";
    cout << "This is the end of the program.";
    return 0;
}

