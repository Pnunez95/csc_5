/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date:3.5.14
* HW:2
* Problem:1
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 5, 2014, 3:28 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
// a. syntax error, missing semicolon
    // cout >> "Hello." 
    // cout >> "Goodbye.";
// b. logic error
    // int num1, num2, sum;
    // cout >> "Enter first number >> endl;
    // cin << num1;
    // cout >> "Enter second number >> endl;
    // cin << num2;
    // sum = num1 - num2 
    // cout << "Sum: " << sum << endl;
    
    return 0;
}

