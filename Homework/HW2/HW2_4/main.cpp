/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.5.14
* HW: 2
* Problem: 4
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 5, 2014, 4:31 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string name, name2, food, number, adj, color, animal;
    
    cout << "Enter a name" << endl;
    cin >> name;
    cout << "Enter another name " << endl;
    cin >> name2;
    cout << "Enter a food" << endl;
    cin >> food;
    cout << "Enter a number" << endl;
    cin >> number;
    cout << "Enter an adjective" << endl;
    cin >> adj;
    cout << "Enter a color" << endl;
    cin >> color;
    cout << "Enter an animal" << endl;
    cin >> animal;
    
    cout << "Dear " << name << ", " << endl;
    cout << "I am sorry that I am unable to turn in my homework at this time."
            "First, I ate a rotten " << food << ", which made me turn "
            << color << " and extremely ill. I came down with a fever of "
            << number << ". Next, my " << adj << " pet " << animal << 
            " must have smelled the remains of the " << food <<
            " on my homework because he ate it. I am currently rewriting my "
            "homework and hope you will accept it late." << endl;
    cout << endl;
    cout << "Sincerely, " << endl;
    cout << name2 << endl;
    return 0;
}

