/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.5.14
* HW: 2
* Problem: 2
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 5, 2014, 4:00 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string name1 = "Calvin";
    string name2 = "Bill";
    
    cout << "Hello my name is " << name1 << endl;
    cout << "Hi there " << name1 << "! My name is " << name2 << endl;
    cout << "Nice to meet you " << name2 << "!\n"
            << "I am wondering if you're available for what?\n";
    cout << "What? Bye " << name1 << "!\n";
    cout << "Uh... sure, bye " << name2 << "!\n\n";
    
    cout << name1 << name1 << name1 << name1 << name1 << endl;
    cout << name2 << name2 << name2 << name2 << name2 << endl;
    return 0;
    /*a. 
Hello my name is Calvin
Hi there Calvin! My name is Bill
Nice to meet you Bill!
I am wondering if you're available for what?
What? Bye Calvin!
Uh... sure, bye Bill!

CalvinCalvinCalvinCalvinCalvin
BillBillBillBillBill
*/
    /*b.
 6
*/
    /*c.
string name1 = "Calvin";
string name2 = "Bill";
*/
    /*d.
     string name1 = "Calvin";
    string name2 = "Bill";
    
    cout << "Hello my name is " << name1 << endl;
    cout << "Hi there " << name1 << "! My name is " << name2 << endl;
    cout << "Nice to meet you " << name2 << "!\n"
            << "I am wondering if you're available for what?\n";
    cout << "What? Bye " << name1 << "!\n";
    cout << "Uh... sure, bye " << name2 << "!\n\n";
    
    cout << name1 << name1 << name1 << name1 << name1 << endl;
    cout << name2 << name2 << name2 << name2 << name2 << endl;
 */
    /*e.
variables store the input from the user and output it back.
*/
}

