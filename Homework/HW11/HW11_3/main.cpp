/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 29, 2014, 11:41 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    class time{
    public:
        int secs;
        int mins;
        int hours;
    } t1;

    struct time2{
        int secs;
        int mins;
        int hours;
    } t2;
    
    cout << "Seconds: " << t1.secs << endl;
    cout << "Minutes: " << t1.mins << endl;
    cout << "Hours: " << t1.hours << endl;;
    
    cout << "Seconds: " << t2.secs << endl;
    cout << "Minutes: " << t2.mins << endl;
    cout << "Hours: " << t2.hours << endl;;
    return 0;
}

