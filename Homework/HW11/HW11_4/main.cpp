/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 29, 2014, 11:56 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    class time{
    public:
        int secs;
        int mins;
        int hours;
    } t1;

    struct time2{
        int secs;
        int mins;
        int hours;
    } t2;
    
    t1.secs = 10;
    t1.mins = 14;
    t1.hours = 2;
    
    t2.secs = 41;
    t2.mins = 24;
    t2.hours = 3;
    
    cout << "Seconds: " << t1.secs << endl;
    cout << "Minutes: " << t1.mins << endl;
    cout << "Hours: " << t1.hours << endl;;
    
    cout << "Seconds: " << t2.secs << endl;
    cout << "Minutes: " << t2.mins << endl;
    cout << "Hours: " << t2.hours << endl;;
    return 0;
}

