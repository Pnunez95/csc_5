/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.15.14
* HW:3
* Problem:1
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 15, 2014, 1:41 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int max_capacity;
    int num_people;
    
    cout << "Enter the number of people in attendance. " << endl;
    cin >> num_people;
    cout << "Enter the max capacity of the room." << endl;
    cin >> max_capacity;
    
    if (num_people <= max_capacity)
    {
        cout << "Meeting is legal. " << max_capacity - num_people
                << " people may still attend." << endl;
    }
    else
    {
        cout << "Meeting is not legal. " << num_people - max_capacity
                << " people must leave." << endl;
    }
    return 0;
}

