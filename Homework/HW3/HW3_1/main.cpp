/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.15.14
* HW:3
* Problem:1
* I certify this is my own work and code
*/
/*
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 15, 2014, 1:28 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string phon_num;
    
    cout << "Enter a telephone number." << endl;
    cin >> phon_num;
    cout << "You entered: " << phon_num;
    return 0;
}

