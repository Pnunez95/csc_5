/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.15.14
* HW:3
* Problem:1
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 15, 2014, 1:41 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int num_apples;
    int num_oranges;
    int num_pears;
    
    cout << "Enter the number of apples" << endl;
    cin >> num_apples;
    cout << "Enter the number of oranges" << endl;
    cin >> num_oranges;
    cout << "Enter the nunmber of pears" << endl;
    cin >> num_pears;
    
    if (num_apples >= 6)
    {
        cout << "The number of apples you should leave is: " 
                << num_apples - 6 << endl;
    }
    else 
    {
        cout << "Not enough apples." << endl;
    }
    
    if (num_oranges >= 6)
    {
        cout << "The number of oranges you should leave is: "
                << num_oranges - 6 << endl;
    }
    else
    {
        cout << "Not enough oranges." << endl;
    }
    
    if (num_pears >= 6)
    {
        cout << "The number of pears you should leave is: "
                << num_pears - 6 << endl;
    }
    return 0;
}

