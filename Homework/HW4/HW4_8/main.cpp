/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:4
* Problem:8
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 3:57 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(0));
    string player1;
    string player2;
    int rand_num1;
    int rand_num2;
    
    cout << "Player one, press r, p, s for rock, paper, or scissors." << endl;
    rand_num1 = rand() % (3 - 1 +1) + 1;
    if(rand_num1 == 1)
    {
        player1 = "r";
    }
    else if(rand_num1 == 2)
    {
        player1 = "p";
    }
    else
    {
        player1 = "s";
    }
    
    cout << "Player two, press r, p, s for rock, paper, or scissors." << endl;
    rand_num2 = rand() % (3 - 1 +1) + 1;
    if(rand_num2 == 1)
    {
        player2 = "r";
    }
    else if(rand_num2 == 2)
    {
        player2 = "p";
    }
    else
    {
        player2 = "s";
    }
    
    
    if(player1 == player2)
    {
        cout << "Nobody wins." << endl;
    }
    
    if(player1 == "r")
    {
        if(player2 == "p")
        {
            cout << "Paper covers rock. Player 2 wins." << endl;
        }
        if(player2 == "s")
        {
            cout << "Rock breaks scissors. Player 1 wins" << endl;
        }
    }
    
    if(player1 == "p")
    {
        if(player2 == "r")
        {
            cout << "Paper covers rock. Player 1 wins." << endl;
        }
        if(player2 == "s")
        {
            cout << "Scissors cut paper. Player 2 wins." << endl;
        }
    }
    
    if(player1 == "s")
    {
        if(player2 == "r")
        {
            cout << "Rock breaks scissors. Player 2 wins." << endl;
        }
        if(player2 == "p")
        {
            cout << "Scissors cut paper. Player 1 wins." << endl;
        }
    }
    return 0;
}

