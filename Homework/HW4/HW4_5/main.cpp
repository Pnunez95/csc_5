/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:4
* Problem:5
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 2:33 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int num_ex;
    int points_recieved;
    int points_possible;
    double total_recieved;
    double total_possible;
    
    cout << "Enter the number of exercises." << endl;
    cin >> num_ex;    
    
    for(int i = 1; i != num_ex + 1; i++)
    {
    cout << "Score recieved for excercise " << i << "?" << endl;
    cin >> points_recieved;
    total_recieved += points_recieved;
    cout << "Score possible for excercise " << i << "?" << endl;
    cin >> points_possible;
    total_possible += points_possible;
    
    if(i == num_ex)
    {
        cout << "Your total is " << total_recieved << " out of "
                << total_possible << " or " 
                << total_recieved / total_possible * 100
                << "%." << endl;
    }
    
    }
    
    
    return 0;
}

