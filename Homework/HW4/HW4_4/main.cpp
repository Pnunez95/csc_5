/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:4
* Problem:4
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 2:01 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int select;
    int weight;    
    int height;
    int age;
    string gender;
    int bmr;
    int num_bars;
    
    do
    {
        cout << "To calculate BMR, press 1. To quit press 2." << endl;
        cin >> select;
        switch(select)
        {
            case 1:
                cout << "Enter weight." << endl;
                cin >> weight;
                
                cout << "Enter height in inches." << endl;
                cin >> height;
                
                cout << "Enter age." << endl;
                cin >> age;
                
                cout << "Enter m if male, f if female." << endl;
                cin >> gender;
                
                if(gender == "m")
                {
                    bmr = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
                }
                else
                {
                    bmr = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
                }
                
                cout << "Your BMR is: " << bmr << "." << endl;
                
                num_bars = bmr / 210;
                
                cout << "You can eat up to " << num_bars
                        << " bars to maintain your body weight." << endl;
                break;
            case 2:
                cout << "Program ended." << endl;
                break;
        }
    }
    while(select != 2);
    return 0;
}

