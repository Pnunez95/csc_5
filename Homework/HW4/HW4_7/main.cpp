/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:4
* Problem:7
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 3:45 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int select;
    string player1;
    string player2;
    
    do
    {
    cout << "To play rock paper scissors with 2 players press 1." << endl;
    cout << "To quit press 2." << endl;
    cin >> select;
    
    switch(select)
    {
        case 1:

    
    cout << "Player one, press r, p, s for rock, paper, or scissors." << endl;
    cin >> player1;
    cout << "Player two, press r, p, s for rock, paper, or scissors." << endl;
    cin >> player2;
    
    if(player1 == player2)
    {
        cout << "Nobody wins." << endl;
    }
    
    if(player1 == "r")
    {
        if(player2 == "p")
        {
            cout << "Paper covers rock. Player 2 wins." << endl;
        }
        if(player2 == "s")
        {
            cout << "Rock breaks scissors. Player 1 wins" << endl;
        }
    }
    
    if(player1 == "p")
    {
        if(player2 == "r")
        {
            cout << "Paper covers rock. Player 1 wins." << endl;
        }
        if(player2 == "s")
        {
            cout << "Scissors cut paper. Player 2 wins." << endl;
        }
    }
    
    if(player1 == "s")
    {
        if(player2 == "r")
        {
            cout << "Rock breaks scissors. Player 2 wins." << endl;
        }
        if(player2 == "p")
        {
            cout << "Scissors cut paper. Player 1 wins." << endl;
        }
    }
    cout << endl;
            
            break;
        case 2:
            cout << "Program ended." << endl;
            break;
    }
    }
    while(select != 2);
    return 0;
}

