/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:4
* Problem:1
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 1:18 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

        int num;
        int total_sum;
        int pos_sum;
        int neg_sum; 
        int total_average;
        int pos_average;
        int neg_average;
        int num_pos;
        int num_neg;
    
    for(int i = 0; i < 10; i++)
    {
        cout << "Enter a number." << endl;
        cin >> num;
        cout << "Number entered: " << num << endl;
        cout << endl;
        
        total_sum += num;
        total_average = total_sum / 10;
        
        if(num > 0)
        {
            pos_sum += num;
            num_pos++;
        }
        else
        {
            neg_sum += num;
            num_neg++;
        }
    }
        pos_average = pos_sum / num_pos;
        neg_average = neg_sum / num_neg;
        
        cout << "Total sum: " << total_sum << endl;
        cout << "Sum of positive numbers: " << pos_sum << endl;
        cout << "Sum of negative numbers: " << neg_sum << endl; 
        cout << "Total average: " << total_average << endl;
        cout << "Average of positive numbers: " << pos_average << endl;
        cout << "Average of negative numbers: " << neg_average << endl;

    return 0;
}

