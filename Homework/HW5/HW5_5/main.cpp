/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 3.30.14
* HW:5
* Problem:5
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 6:24 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
void calc_mpg()
{
    double mpg;
    double miles_traveled;
    double gallons;
    double num_liters;
    const double LITER = 0.264179;
    
    cout << "Enter number of liters." << endl;
    cin >> num_liters;
    cout << "Enter miles traveled." << endl;
    cin >> miles_traveled;     
    
    gallons = LITER * num_liters;
    mpg = miles_traveled / gallons ;
    
    cout << "Your car gets " << mpg;
    cout << " miles per gallon." << endl;   
    cout << endl;
}
int main(int argc, char** argv) {
 
    int select;
    
    do
    {
        cout << "To convert liters to gallons and calculate mpg press 1." << endl;
        cout << "To end program press 2" << endl;
        cin >> select;
        
        switch(select)
        {
            case 1:
                calc_mpg();
                break;
            case 2:
                cout << "End of program." << endl;
                break;
        }
    }
    while(select != 2);
    return 0;
}

