/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 18, 2014, 11:44 AM
 */

#include <cstdlib>
#include <iostream> // Adds 1 i/o to my program
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Hello World" << endl; // semicolons denotes end of statement
    return 0;
}

