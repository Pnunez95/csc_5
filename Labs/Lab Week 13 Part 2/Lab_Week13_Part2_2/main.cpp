/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 22, 2014, 11:42 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int **create_multidimensional_array(int row, int col)
{
	int **p = new int*[row];
	for(int i = 0; i < row; i++)
	{
		p[i] = new int[col];
	}
	return p;
}
void output(int **p, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
			cout << p[i][j] << " ";
			cout << endl;
	}
}
void fill_value(int **p, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			p[i][j]= i * col + j + 1;
		}
	}
}
int main(int argc, char** argv) {

    cout << "Enter number of rows." << endl;
    int row;
    cin >> row;
    cout << "Enter number of columns." << endl;
    int col;
    cin >> col;
    int **p = create_multidimensional_array(row, col);
    fill_value(p, row, col);
    output(p, row, col);
    
    int stop = 0;
    while(stop != -1)
    {
        cout << "Add another row?" << endl;
        string resp;
        cin >> resp;
        if(resp == "yes" ||resp =="Yes" || resp == "y" || resp == "Y")
        {
            
            for(int i = 0; i < row; i++)
            {
                    delete[] p[i];
            }
            
            delete [] p;
            row += 1;
            p = create_multidimensional_array( row, col);
            fill_value(p, row, col);
            output(p, row, col);
        }
        else
        {
            stop = -1;
        }
    }
    
//    for(int i = 0; i < row; i++)
//    {
//            delete[] p[i];
//    }
//    delete[] p;

    return 0;
}

