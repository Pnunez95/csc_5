/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 22, 2014, 11:38 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int **create_multidimensional_array(int row, int col)
{
	int **p = new int*[row];
	for(int i = 0; i < row; i++)
	{
		p[i] = new int[col];
	}
	return p;
}


void fill_value(int **p, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			p[i][j]= i * col + j + 1;
		}
	}
}

void output(int **p, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
			cout << p[i][j] << " ";
			cout << endl;
	}
}
int main(int argc, char** argv) {

    int row = 3;
    int col = 3;
    int **p = create_multidimensional_array(row, col);
    fill_value(p, row, col);
    output(p, row, col);
    
    for(int i = 0; i < row; i++)
    {
            delete[] p[i];
    }
    delete[] p;
    return 0;
}

