/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 12:17 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(0));
    int rand_num;
    int min = 1;
    int max = 100;
    int guess;
    int counter;
    
    rand_num = rand() % (max - min + 1) + min;
    
    cout << "Enter a guess between 1 and 100." << endl;    
    cin >> guess;
    while(guess != rand_num && counter!= 10)
    {
        counter ++;
        do
        {
            cout << "Guess is too low." << endl;
            cin << guess;
        }
        while(guess < rand_num);
        do
        {
            cout << "Guess is too high." << endl;
            cin << guess;
        }
        while(guess > rand_num);
    }
    return 0;
}

