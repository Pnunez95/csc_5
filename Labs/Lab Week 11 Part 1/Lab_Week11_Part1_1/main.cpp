/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 6, 2014, 11:33 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <time.h>
using namespace std;

/*
 * 
 */
void fill_vect(vector<int> &v);
void fill_array(int a[], int size);
int main(int argc, char** argv) {
    
    srand(time(0));
    
    vector<int> v(10);
    int a[10];
    
    fill_vect(v);
    fill_array(a,10);
    
    for(int i = 0; i < 10; i++)
    {
        cout << v[i] << " ";
    }
    
    cout << endl;
    
    for(int i = 0; i < 10; i++)
    {
        cout << a[i] << " ";
    }
    
    return 0;
}
void fill_vect(vector<int> &v)
{
    for(int i =0; i < 10; i++)
    {
        v[i] = rand();
    }    
}

void fill_array(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        a[i] = rand();
    }    
}
