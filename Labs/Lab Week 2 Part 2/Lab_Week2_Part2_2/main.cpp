/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:40 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int x, y;
    cout << "Enter an integer less than 1000" << endl;
    cin >> x;
    if (x > 1000)
    {
        x = 999;
    }
    
    cout << "Enter another integer less than 1000" << endl;
    cin >> y;
    if (y > 1000)
    {
        y = 999;
    }
    
    cout << " X = " << x << "   Y = " << y << endl;
    cout << setw(160) << setfill('-') << "\n";
    int temp = x;
    x = y;
    y = temp;
    cout << " X = " << x << "   Y = " << y << endl;
    return 0;
}

