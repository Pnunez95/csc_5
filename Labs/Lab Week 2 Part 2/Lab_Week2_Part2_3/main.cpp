/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:39 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int x = 12;
    int y = 5;
    
    if (x == 7)
    {
        y = 4;
    }
    else if(x == 9)
    {
        y = 3;
    }
    else
    {
        y = 6;
    }
    cout << y;
    return 0;
}

