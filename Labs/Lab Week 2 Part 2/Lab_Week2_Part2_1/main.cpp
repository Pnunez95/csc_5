/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 12:06 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    double singles;
    double doubles;
    double triples;
    double home_runs;
    double at_bats;
    double slugging_percentage;
    
    cout << "Enter number of singles" << endl;
    cin >> singles;
    cout << "Enter number of doubles" << endl;
    cin >> doubles;
    cout << "Enter number of triples" << endl;
    cin >> triples;
    cout << "Enter the number of home runs" << endl;
    cin >> home_runs;
    cout << "Enter the number of at bats" << endl;
    cin >> at_bats;
    
    slugging_percentage = singles + 2 * doubles + 3 * triples + 4 * home_runs
            / at_bats;
    
    cout << "Slugging Percentage: " << slugging_percentage << endl;
    return 0;
}

