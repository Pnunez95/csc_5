/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 24, 2014, 12:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int end = 0;
    vector<int> numbers;
    
    while(end != -1)
    {
        cout << "Enter a number." << endl;
        int num;
        cin >> num;
        
        if(num == -1)
        {
            end = -1;
        }
        else
        {
        numbers.push_back(num);    
        }
    }
    
    for(int i = 0; i < numbers.size(); i++)
    {
        cout << numbers[i] << endl;
    }
    return 0;
}

