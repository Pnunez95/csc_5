/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 12:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * 
 */
int str_size(ifstream infile)
{
    string sent;
    int characters;
    infile.open("test.txt");
    
    while(!infile.eof())
    {
        getline(infile, sent);
        characters += sent.size();
    }
    infile.close();
}
int main(int argc, char** argv) {

    ifstream infile;
    infile.open("test.txt");
    cout << str_size(infile) << " number of characters in file." << endl;
    infile.close();
    return 0;
}

