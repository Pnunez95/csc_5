/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 11:54 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int triple(int num)
{
    return num * 3;
}
int main(int argc, char** argv) {

    int num;
    cout << "Enter a number." << endl;
    cin >> num;
    
    cout << triple(num);
    return 0;
}

