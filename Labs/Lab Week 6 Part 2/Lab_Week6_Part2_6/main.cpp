/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 12:06 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int str_size(string sent)
{
    return sent.size();
}
int main(int argc, char** argv) {

    string sent;
    cout << "Enter anything." << endl;
    cin >> sent;
    cout << "Length of string is: " << str_size(sent) << endl;
    return 0;
}

