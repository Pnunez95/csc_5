/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 12:01 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int square(int num)
{
    return num * num;
}
int main(int argc, char** argv) {

    int num;
    cout << "Enter a number." << endl;
    cin >> num;
    cout << "Square is: " << square(num) << endl;
    return 0;
}

