/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 29, 2014, 10:38 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    ifstream infile;
    
    infile.open("test.txt");
    
    string data;
    int characters;
    
    while(!infile.eof())
    {
        getline(infile, data);
        characters = data.size();
        
        if(infile.eof())
        {
            cout << "Number of characters: " << characters << endl;
            
            ofstream outfile;
            
            outfile.open("characters.txt");
            
            outfile << characters << endl;
            outfile.close();
        }
    }
    infile.close(); 
    return 0;
}

