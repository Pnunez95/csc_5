/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 30, 2014, 11:01 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int n;
    cin >> n;
    double x = 0;
    double s;
    do
    {
        s = 1.0 / (1 + n * n);
        n++;
        x = x + s;
        cout << x << endl;
    }
    while(s > 0.01);
    
    int n2;  
    cin >> n2;    
    double x2 = 0;
    double s2;   
    
    while(s2 > 0.01)
    {
        s2 = 1.0 / (1 + n2 * n2);
        n2++;
        x2 = x2 + s2;        
        cout << x2 << endl;
    }
    return 0;
}

