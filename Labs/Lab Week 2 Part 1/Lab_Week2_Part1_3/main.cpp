/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 12:37 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int test1;
    int test2;
    int test3;
    int average;
    
    /*cout << "Enter test 1 score" << endl;
    cin >> test1;
    cout << "Enter test 2 score" << endl;
    cin >> test2;
    cout << "Enter test 3 score" << endl;
    cin >> test3;
    average = test1 + test2 + test3 / 3;
    cout << "Test average: " << average << "." << endl;
    */
    
    cout << "Enter test scores " << endl;
    cin >> test1;
    cin >> test2;
    cin >> test3;
    average = test1 + test2 + test3 / 3;
    cout << "Your average is " << average << endl;
    return 0;
}

