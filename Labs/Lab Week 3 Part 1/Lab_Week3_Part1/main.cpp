/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //get the amount owed
    cout << "Enter how much you owe." << endl;
    double owe;
    cin >> owe;
    
    //Get the amount paid
    cout << "How much did you pay?" << endl;
    double tender;
    cin >> tender;
    
    double change = tender - owe;
    
    if(change < 0) // didn't pay enough
    {
        cout << "You need to give more money" << endl;
    }
    
    //create constants to represent currency
    const int  DOLLAR = 100;
    const int QUARTER = 25;
    const int DIME = 10;
    const int NICKEL = 5;
    const int PENNY = 1;
    
    //convert to pennies
    change *= 100;
    //convert data type to integer
    int change_in_pennies = static_cast<int>(change + .05);
    cout << "Change in Pennies: " << change_in_pennies << endl; 
    
    int num_dollars = change_in_pennies / DOLLAR;
    change_in_pennies %= DOLLAR; 
    
    int num_quarters = change_in_pennies / QUARTER;
    change_in_pennies %= QUARTER;
    
    int num_nickels = change_in_pennies / NICKEL;
    change_in_pennies %= NICKEL;
    
    int num_pennies = change_in_pennies;

    cout << "Dollars: " << num_dollars << endl;
    cout << "Quarters: " << num_quarters << endl;
    cout << "Nickels: " << num_nickels << endl;
    cout << "Pennies: " << num_pennies << endl;
    return 0;
}

