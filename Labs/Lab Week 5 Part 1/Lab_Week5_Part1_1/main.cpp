/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 29, 2014, 8:09 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    
    int select; 
    double balance;
    double interest_over;
    double interest_under;
    double w_interest;

    
    cout << "To calculate minimum payment press 1." << endl;
    cout << "To quit press 2" << endl;
    cin >> select;
    
    do
    {
        switch(select)
        {
                case 1:
                 cout << "Enter account balance." << endl;
                 cin >> balance;
                 
                 if(balance <= 1000.00)
                 {
                        w_interest = (balance * .015) + balance;
                 }
                 else
                 {
                        w_interest = (1000 * .015) + (balance - 1000) * .01 
                                + balance;
                 }
    
                if(w_interest <= 10.00)
                {
                        cout << "Minimum payment is: $" << w_interest << endl;
                }
    
                double ten_percent;
                ten_percent = w_interest * .10;
    
                if(ten_percent > 10.00)
                {
                        cout << "Minimum payment is: $" << ten_percent << endl;
                }
                else
                {
                        cout << "Minimum payment is: $10." << endl;
                }
                break;
                case 2:
                cout << "End of program." << endl;
                break;
        }
    }
    while(select != 2);
    

    return 0;
}

