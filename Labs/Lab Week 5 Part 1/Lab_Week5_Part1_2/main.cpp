/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 29, 2014, 9:06 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int pile = 23;
    int removed;
    int comp_removed;
    
    do
    {
        if(pile == 1)
        {
            pile -= 1;
            cout << "You can only take the last toothpick. You lose." << endl;
        }        
        
        cout << "There are " << pile << " toothpicks remaining."
                << " Remove 1, 2, or 3." << endl;
        cin >> removed;
        pile -= removed;
        if(removed > 3)
        {
            removed = 3;
        }

        
        if(pile > 4)
        {
            comp_removed = 4 - removed;
            pile -= comp_removed;
            cout << "Computer removed " << comp_removed << "." << endl;
        }
        if(pile >= 2 && pile <=4)
        {
            comp_removed = pile - 2;
            pile -= comp_removed;
            cout << "Computer removed " << comp_removed << "." << endl;
        }
        if(pile == 1)
        {
            pile -= 1;
            cout << "Comp takes final toothpick. You win." << endl;
        }
        
    }
    while(pile != 0);
    
    
    return 0;
}

