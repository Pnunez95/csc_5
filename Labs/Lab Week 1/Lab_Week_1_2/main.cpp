/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 11:19 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int a = 5;
    int b = 10;
    
    //need a temporary variable to store the value of a
    //a's value gets reassigned is never stored anywhere else
    int temp = a;
    
    cout << "a: " << a << " " << "b: " << b << endl;
    
    a = b;
    b = a;
    
    cout << "a: " << a << " " << "b: " << temp << endl;

    return 0;
}

