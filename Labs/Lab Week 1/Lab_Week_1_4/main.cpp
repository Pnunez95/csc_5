/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 12:33 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    double num_meters; 
    const double MILE = 1609.344;
    const double FEET = 3.281;
    const double INCH =12;
    
    cout << "Enter number of meters." << endl;
    cin >> num_meters;
    cout << endl;
    cout << num_meters << " is ";
    cout << num_meters / MILE;
    cout << " miles, ";
    cout << num_meters * FEET;
    cout << " feet, and ";
    cout << num_meters * FEET * INCH;
    cout << " inches.";
    return 0;
}

