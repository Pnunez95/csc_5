/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 20, 2014, 12:00 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string grade;
    
    cout << "Enter a grade" << endl;
    cin >> grade;
    
    if(grade == "A+")
    {
        cout << "100+" << endl;
    }
    
    if(grade == "A")
    {
        cout << "93-100" << endl;
    }
    
    if(grade == "A-")
    {
        cout << "90-92.9" << endl;
    }
    
    if(grade == "B+")
    {
        cout << "87-88.9" << endl;
    }
    
    if(grade == "B")
    {
        cout << "83-86.9" << endl;
    }
    
    if(grade == "B-")
    {
        cout << "80-82.9" << endl;
    }
    return 0;
}

