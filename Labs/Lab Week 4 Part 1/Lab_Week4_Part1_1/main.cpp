/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 11:53 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int value;
    
    cout << "Enter a value between 1 - 100" << endl;
    cin >> value;
    
    if (value > 100)
    {
        value = 100;
    }
    
    if (value < 0)
    {
        value = 0;
    }
    
    if( value >= 90)
    {
        cout << "Grade is: A" << endl;
    }
    else if( value >= 80)
    {
        cout << "Grade is: B" << endl;
    }
    else if( value >= 70)
    {
        cout << "Grade is: C" << endl;
    }
    else if( value >= 60)
    {
        cout << "Grade is: D" << endl;
    }
    else
    {
        cout << "Grade is: F" << endl;
    }
    return 0;
}

