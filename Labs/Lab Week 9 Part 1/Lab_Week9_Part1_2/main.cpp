/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 12:09 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    char sent;
    cout << "Enter a sentence." << endl;
    cin >> sent;
    
    ofstream outfile;
    
    outfile.open("output.dat");
    
    while(sent != "@")
    {
        outfile << cin.get(sent);
    }
    
    outfile.close();
    
    return 0;
}

