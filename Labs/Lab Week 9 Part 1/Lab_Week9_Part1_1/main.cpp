/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 11:43 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string sent;
    cout << "Enter a sentence." << endl;
    cin >> sent;
    
    ofstream outfile;
    
    outfile.open("output.dat");
    
    outfile << sent;
    getline(cin, sent);
    int end = sent.find("@");
    sent = sent.substr(0, end);
    outfile << sent;
    
    outfile.close();
    
    return 0;
}

