/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 20, 2014, 12:13 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int year;
    
    cout << "Enter a year" << endl;
    cin >> year;
    
    if(year % 4 == 0)
    {
        if(year % 400 == 0 || year % 100 != 0)
        {
            cout << "Year is a leap year." << endl;
        }
        else
        {
            cout << "Year is a leap year." << endl;
        }
        
    }
    else
    {
        cout << "Year is not a leap year" << endl;
    }
    return 0;
}

