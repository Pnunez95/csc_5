/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 11:47 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(0));
    int rand_num;
    int min = 1;
    int max = 100;
    int guess;
    int counter = 1;
    
    rand_num = rand() % (max - min + 1) + min;
    
    cout << "Enter a guess between 1 and 100." << endl;    
    cin >> guess;
    while(guess != rand_num && counter!= 10)
    {
        counter ++;      
        cout << "Attempt Number: " << counter << endl;  

        if(guess < rand_num)
        {

            cout << "Guess is too low." << endl;
            cin >> guess;
        }
        else if (guess > rand_num)
        {
            cout << "Guess is too high." << endl;
            cin >> guess;
        }
        else if(guess == rand_num)
        {
            cout << "Number is correct. You win." << endl;
        }
    }
    
    if(guess == rand_num)
    {
        cout << "You win!" << endl;
    }
    
    if(counter == 10)
    {
        cout << "You lose. Game over." << endl;
    }
    
    return 0;
}

