/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 13, 2014, 12:18 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string grade;
    string plus = "+";
    string minus = "-";
    cout << "Enter grade" <<endl;
    cin >> grade;
    
    if( grade.substr(0,1)== "A")
    {
        if(grade.substr(1,1) == "+")
        {
            cout << "Number: 4.0" << endl;
        }
        else if(grade.substr(0,1) == "-")
        {
            cout << "Number: 3.7" << endl;
        }
        else
        {
            cout << "Number: 4.0" << endl;
        }
    }
    
    if( grade.substr(0,1) == "B")
    {
        if(grade.substr(1,1)== "+")
        {
            cout << "Number: 3.3" << endl;
        }
        else if(grade.substr(0,1) == "-")
        {
            cout << "Number: 2.7" << endl;
        }
        else
        {
            cout << "Number: 3.0" << endl;
        }
    }
    return 0;
}

