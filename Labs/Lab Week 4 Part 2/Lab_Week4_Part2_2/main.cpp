/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 20, 2014, 12:09 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int n;
    double guess1;
    double guess2;
    double r;
    
    cout << "Enter a number" << endl;
    cin >> n;

    guess2 = n / 2;
    guess1 = n;
    
    while( guess2 / guess1 > .99)
    {
        r = n / guess2;
        guess1 = guess2;
        guess2 = (guess2 + r) / 2;
    }
    
    cout << "The square root of " << n << " is " << static_cast<double>(guess2) 
            << "." << endl;
    
    
    return 0;
}

