/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 27, 2014, 10:52 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // const variable to a number
    const double rand_num = 12.3259;
    
    //error. can't reassign constant values
    //rand_num = 12.5;
    
    //change number of numbers to 2
    //no fixed means no decimals
    cout << setprecision(2);
    
    cout << rand_num << endl;
    //fixing means change decimal places
    
    //output a three digit number
    //only see two digits with scientific notation
    cout << 120.528 << endl;
    
    cout << fixed << rand_num << endl;
    
    //use set width to create columns
    //set w() only manipulates next output
    //set w() automatically right justifies
    cout << setw(10) << rand_num << rand_num << endl;
    
    //left justification
    //setw() reserves space for the next output
    cout << setw(10) << left << rand_num << rand_num << endl;
    
    
    cout << setw(10) 
         << setfill('=') 
         << rand_num 
         << rand_num;
    return 0;
}

