/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //delcares two variables of type integer
    int num1, num2;
    
    //Output junk values
    cout << num1 << " " << num2 << endl;
    
    //initialization
    //num1 = num2 = 0;
    num1 = 0;
    num2 = 0;
    
    //output variables again
    cout << num1 << " " << num2 << endl;
    
    //get user input for both values
    cout << "Please enter two integers" << endl;
    cin >> num1 >> num2;
    cout << "You entered: "<< endl;
    cout << num1 << " " << num2 << endl;
    
    //calculate average of two numbers
    int total = num1 + num2;
    cout << endl<< "Total: " << total << endl;
    
    double average_int_division = total / 2;
    
    double average_static_cast = static_cast<double>(total) / 2;
    double average_decimal = total / 2.0;
    
    cout << "Average: " << average_int_division << endl; //output average
    
    cout << "Average Static Cast: " << average_static_cast << endl;
    cout << "Average Decimal: " << average_decimal << endl;
    return 0;
}

