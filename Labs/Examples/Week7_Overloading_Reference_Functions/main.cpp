/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 1, 2014, 11:52 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

//create a global constant for pi
//global because outside of main
const double PI = 3.14;

//function prototype/declaration
double circumference(double);
double area(double);

/*
 * 
 */
//function prototype for swap
void swap_non_referenced(double, double);

void swap_referenced(double&, double&);

void output(double, double);

int main(int argc, char** argv) {

    cout << "Enter a radius. " << endl;
    int radius;
    cin >> radius;
    
    cout << "Circumference is: " << circumference(radius) << endl;
    cout << "Area is: " << area(radius) << endl;
    
    //swap function non referenced
    cout << "Enter 2 values" << endl;
    double x, y;
    cin >> x >> y;
    
    output(x,y);
    swap_non_referenced(x, y);
    output(x,y);
    
    output(x,y);
    swap_referenced(x,y);
    output(x,y);
    return 0;
}

//define all my functions that are prototyped
/*
 * functions comments need 3 key details
 * 1.(high level view describing what the function is supposed to do
 * circumference function that calculates the circumference of a circle
 * 
 * 2.(return type) function returns the circumference of a 
 * given radius of type double
 * 
 * 3.(parameters)one parameter of type double that represents the 
 * radius of a circle
 * 
 * 
 */

double circumference(double r)
{
    return 2 * PI * r;
}

/*
 * 1.Area function returns the area of a circle with a given radius
 * 
 * 2.returns a double representing the area of a circle
 * 
 * 3.one parameter that is the radius of a circle of type double
 */
double area(double r)
{
    return PI * r * r;
}

/*
 * swap_non_reference attempts to swap two values without referencing parameters
 * 
 * parameters: two doubles representing values to be swapped
 * 
 * returns nothing 
 */
void swap_non_referenced(double x, double y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 * swap_reference attempts to swap two values while referencing parameters
 * 
 * parameters: two doubles representing values to be swapped
 * 
 * returns nothing 
 */
void swap_referenced(double &x, double &y)
{
    int temp = x;
    x = y;
    y = temp;
}

/*
 * outputs two double values to the console
 */
void output(double x, double y)
{
    cout << "First is: " << x
            << "Second is: " << y << endl;
}
