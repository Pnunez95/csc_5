/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 13, 2014, 9:36 AM
 */

#include <cstdlib>
#include <iostream>
#include <ctype.h>
using namespace std;

// Prototypes
// Get the word from the user
string getWord();

// Clean the word
string clean(const string & s, const string & punct);

// Fix case
void fixCase(string &);

/*
 * 
 */
int main(int argc, char** argv) {

    // Invoke getWord function
    string s = getWord();
    
    // Constructor
    string punct("1234567890<>,.;:'\"[]{}\\|$&");
    
    // Output after
    
    // call clean function
    string cleanWord = clean(s, punct);
    cout << "CLEAN WORD: " << cleanWord << endl;
    
    // call case function
    fixCase(cleanWord);
    
    cout << "FIXED CASE: " << cleanWord << endl;
    
    return 0;
}

/*
 * getWord function gets a single word from the user
 */
string getWord()
{
    // Prompt the user
    cout << "Please enter a word" << endl;
    string s;
    cin >> s;
    return s;
}

/*
 * Clean function attempts to remove punctuation
 * and numerical values from a given string
 * 
 * Returns a "clean" version of a string
 * 
 * Uses two parameters, const string &s, 
 * and const string & punct.
 * s is the original string
 * punct is the string of bad characters
 */
string clean(const string &s, const string & punct)
{
    string cleanWord = ""; // Null string
    
    // Check every character
    for (int i = 0; i < s.size(); i++)
    {
        // Check if character is bad
        // aka contained in list of bad character
        if (punct.find(s[i]) == -1)
        {
            cleanWord += s[i];
        }
    }
    return cleanWord;
}

/*
 * fixCase lowers all the characters to lower case
 * 
 * Returns nothing
 * 
 * Uses one string passed by reference
 */
void fixCase(string &s)
{
    for(int i = 0; i < s.size(); i++)
    {
        s[i] = tolower(s[i]);
    }
}