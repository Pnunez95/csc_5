/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 20, 2014, 9:49 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    ifstream in_file;
    string data;
    
    in_file.open("hello.txt");
    
    if(in_file.is_open() == true)
    {
        cout << "File is open" << endl;
    }
    else
    {
        cout << "File is not open" << endl;
    }
    
    while(!in_file.eof())
    {
        //in_file >> data;
        getline(in_file, data);
        cout << data << endl;
    }
    in_file.close();
    return 0;
}

