/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 22, 2014, 10:25 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int **create_multidimensional_array(int row, int col)
{
	//create pointer to pointer array
	int **p = new int*[row];
	//iterate through all rows
	for(int i = 0; i < row; i++)
	{
		p[i] = new int[col];
	}
	return p;
}


void fill_value(int **p, int row, int col) //need size
{
	//use nested for loops
	//row
	for(int i = 0; i < row; i++)
	{
		//col
		for(int j = 0; j < col; j++)
		{
			p[i][j]= i * col + j + 1;
		}
	}
}

void output(int **p, int row, int col)
{
	//row
	for(int i = 0; i < row; i++)
	{
		//col
		for(int j = 0; j < col; j++)
			cout << p[i][j] << " ";
			cout << endl;
	}
}
int main(int argc, char** argv) {

    int row = 5;
    int col = 3;
    int **p = create_multidimensional_array(row, col);
    fill_value(p, row, col);
    output(p, row, col);
    return 0;
}

