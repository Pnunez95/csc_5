/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 10:46 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream> //file input/ output
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //create object/declare variable
    //variable is not the object, variable is representation of the object
//    ifstream infile;
//    
//    //step one, open file
//    infile.open("data.dat");
//    
//    //step two, read from file
//    string word;
//    infile >> word;
//    
//    //step three, close file
//    infile.close();
//    
//    cout << "Your word is: " << word << endl;
//    
    //use the while loop
    ifstream infile;
    
    infile.open("data.dat");
    
    //use while loop to extract words until the end of file
    string word;
    while(infile >> word) // easy way to get words
    {
        cout << "Your word is: " << word << endl;
    }
    
    //output to a file, use of streamobject
    ofstream outfile;
    
    outfile.open("test.txt");
    
    outfile << "Paul Nunez" << endl;
    outfile << "Hello World" << endl;
    
    outfile.close(); // close file
    
    
    return 0;
}

