/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 18, 2014, 11:28 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //seed
    //provide a time to the random number generator
    //srand returns no value
    srand(time(0));
    //create a random number
    int rand_num = rand();
    
    cout << "Number is: " << rand_num << endl;
    
    //get a range between 0 and 100 
    int rand_num2 = rand() % 101;
    
    cout << "Number 2 is: " << rand_num2 << endl;
    
    //get a range between 100-1000
    //store everything in variables
    int max = 1000;
    int min = 100;
    
    int rand_num3 = rand() % (max - min + 1) + min;
    
    cout << "Number 3 is: " << rand_num3 << endl;
    
    //find out how many iterations it takes to get the number 1000
    //use a counter to count the iterations
    int counter = 1;
    while(rand_num3 != 1000)
    {
        counter ++; 
        cout << "Iteration: " << counter << endl;
        
        //get new random number
        rand_num3 = rand() % (max - min + 1) + min;
    }
    return 0;
}

