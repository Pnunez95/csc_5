/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 29, 2014, 12:11 PM
 */

#include <cstdlib>
#include <iostream>
#include <time.h>
using namespace std;

/*
 * 
 */
void rand_fifty(int a[]);

int main(int argc, char** argv) {

    int a[50];
    
    rand_fifty(a);
    
    return 0;
}

void rand_fifty(int a[])
{
    srand(time(0));
    for (int i =0; i < 50; i++)
    {
        a[i] = rand();
    }
    
    for(int i = 0; i < 50; i++)
    {
        cout << a[i] << " ";
    }
}
