/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 24, 2014, 11:17 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

// Get number function gets a number from the user
int getNumber()
{
    cout << "Please enter a number: " << endl;
    int num;
    cin >> num;
    return num;
}
/*
 * 
 */
int main(int argc, char** argv) {

    // Get 10 values from the user
    // and insert it into a vector
    // Method 1 - Empty Vector
    vector<int> emptyVector;
    
    // Output vector before 
    for (int i = 0; i < emptyVector.size(); i++)
    {
        cout << emptyVector[i] << endl;
    }
    
    // Always use for loops for vectors
    for (int i = 0; i < 10; i++)
    {
        // Write a function for user input
        int num = getNumber();
        emptyVector.push_back(num);   
    }
    
    // Output vector before 
    for (int i = 0; i < emptyVector.size(); i++)
    {
        cout << emptyVector[i] << endl;
    }
    
    // Method 2 - Vector of known size
    vector<int> v(10); // v has size 10 
    
    cout << endl << "Before vector: " << endl;
    for(int i = 0; i < v.size(); i++)
    {
        // Using subscript operator
        cout << v[i] << endl;
    }
    
    for (int i = 0; i < v.size(); i++)
    {
        v[i] = getNumber();
    }
    
    cout << endl << "After vector: " << endl;
    for(int i = 0; i < v.size(); i++)
    {
        // Using subscript operator
        cout << v[i] << endl;
    }
    return 0;
}