/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 27, 2014, 10:04 AM
 */

#include <cstdlib> // atoi and atod
#include <iostream>
#include <ctype.h> // isdigit function

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Create char array/ const pointer
    // Can also use strings
    string c;
    
    do
    {
        cout << "Please enter a number: " << endl;
        
        cin >> c; // Get user input
        
        // Check if input starts with a number
        // Need to pass in a const char value
        if (isdigit(c[0])) 
        {
            // If using strings, convert to c-string
            int num = atoi(c.c_str());
            cout << "Your number is: " << num << endl;
        }
        else
        {
            cout << "No number was entered" << endl;
        }
    } while (true);
    
    return 0;
}