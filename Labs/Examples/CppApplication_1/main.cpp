/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on March 29, 2014, 9:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Enter word." << endl;
    string word;
    cin >> word;
	bool b = true;
	for(int i = 0; i < word.size(); i++)
	{
		if(word[i] != word[word.size() - 1 - i])
		{
			b = false;
		}
	}
	
	if(b)
	{
		cout << "Palindrome" << endl;
	}	
	
	else
	{
	cout << "Not Palindrome" << endl;
	}
    return 0;
}
