/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2014, 10:54 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Use the cin.get() function to get a single
    // character
    char c;
    cout << "Enter a character: " << endl;
    c = cin.get(); // or cin.get(c);
    cout << "Your character is: " << c << endl;
    
    
    // Use getline to get a sentence from a user
    // getline reads until the newline character
    string sentence;
    cout << "Please enter a sentence" << endl;
    
    // Getline needs a buffer, and a string
    // Getline ignores the new line character
    getline(cin, sentence);
    cout << "Your sentence is: " << sentence << endl 
            << "end" << endl;
    
    // Stream insertion
    // Stream insertion leaves the newline in the buffer
    // Try not to mix >> and getline
    cout << "Please enter a word: " << endl;
    string word;
    cin >> word;
    cout << "Your word is: " << word << endl;
    
   
    // Cleaning/fixing the buffer
    while (true)
    {
        int number;
        cout << "Enter a number: ";
        
        cin >> number;
          
        // Strings break the buffer
        // Need to fix and reset the cin buffer
        if (cin.fail()) // Need to check if buffer broke
        {
            // Need to reset the buffer
            // Need to clear the buffer of its contents
            cin.clear(); // resets the buffer
            
            // ingore (clear) the first 256 characters
            // or until a newline is found
            cin.ignore(256, '\n');
        }
        else
        {
            cout << "Your number is: " << number << endl;
        }
    }
    return 0;
}