/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 29, 2014, 11:38 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

/*
 * 
 */
//prototype for output function
void output (const vector<int> &);
//prototype for outputting an array
void output (int[], int);
int main(int argc, char** argv) {
    
    vector<int> v; //empty vector
    
    output(v); // output an empty vector
    //Lab week 9 part 2
    v.push_back(3);
    v.push_back(6);
    v.push_back(8);
    
    output(v);
    
    //create an array
    int a[10];
    output(a, 10);
    
    int b[10] = {1, 2, 3};
    output(b, 10);
    
    //illegal
    //cant change memory location of array variables
    //a = b;
    
    cout << endl;
    
    int num;
    cout << "Please enter a number" << endl;
    cin >> num;
    //can assign a size for an array for the first time
    //is known as variable length array declaration
    int c[num];
    output (c, num);
    return 0;
}
/*
 * the output function outputs the contents of a vector of type integer
 * 
 * returns void
 * 
 * parameter is one vector of type integer
 */
void output(const vector<int> &v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}
/*
 * output function attempts to ouput an array ofints
 * parameters: array of ints, size of array
 * returns void
 */
void output(int a[], int size)
{
    for(int i =0; i < size; i++)
    {
        cout << a[i]  << " ";
    }
}