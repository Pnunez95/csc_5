/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2014, 11:14 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int num;
    
    // prompt the user for input 
    cout << " Enter an integer " << endl;
    cin >> num;
    
    // determine if a number is positive or negative
    if (num > 0) // positive
    {
        cout << "You entered a postiive number" << endl;
    }
    else if (num < 0) // negative
    {
        cout << "You entered a negative number" << endl;
    }
    else
    {
        cout << "You entered the number 0" << endl;
    }
    
    // switch cases
    
    int switch_num;
    cout << "Enter a number between 1-3" << endl;
    cin >> switch_num;
    
    // switch acts like a menu
    switch (switch_num)
    {
        case 1:
            cout << "You entered the number 1" << endl;
            break;
        case 2:
            cout << "You entered the number 2" << endl;
            break;
        case 3: 
            cout << "You entered the number 3" << endl;
            break;
        default:
            cout << "You did not enter a valid number" << endl;
            break;
    }
    //while loops
    //needs initialization, condition, and incrementor
    int x = 0;
    while (x < 100000000)
    {
        cout << x << endl;
        x++;
    }
    return 0;
}

