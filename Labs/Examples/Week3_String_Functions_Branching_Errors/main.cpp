/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2014, 11:38 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //variable message is a string
    string message = "Hello World";
    
    //output substring 2,4
    //expect to see llo space
    cout << message.substr(2,4) << endl;
    
    //substring at 7
    //expect to see orld
    //0-based location
    cout << message.substr(7) << endl;
    
    cout << message.find("llo") << endl;\
    
    //should not find anything
    /*
    cout << message.find("dog") << endl;\
    
    int large = large = 4294967295;
    large ++;
    cout << large << endl;
    */
    
    cout << message.length() << endl;
    //syntax error
    // ex. cout << message.size << endl;
    
    //logic error
    cout << "Size: " << message.size() - 1 << endl;
    
    //branching
    int x = 3;
    
    //condition is x <5
    if (x < 5)
    { //code within curly braces
        x = 5;
    }
    cout << "x: " << x << endl;
    
    //either if or else code will be executed. not both
    if (x!= 5) // if x is not equal to 5
    {
        x++;
    }
    else
    {
        x--;
    }
    cout << "x: " << x << endl;
    return 0;
}

