/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 24, 2014, 11:36 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * fiboRecur attempts to recursively 
 * calculate the fibonacci
 * sequence of a given value
 * Takes on argument as the desired sequence
 * Returns the value at a specific sequence as an int
 */
int fiboRecur(int sequence)
{
    // Base case
    cout << "Sequence: " << sequence << endl;
    if (sequence <= 2)
    {
        return 1;
    }
    else // Recursive call
    {
        return fiboRecur(sequence - 1) + 
                fiboRecur(sequence - 2);
    }
}
        
/*
 * 
 */
int main(int argc, char** argv) {

    while(true)
    {
        // Get a number from the user
        cout << "Enter a number: " << endl;
        int num;
        cin >> num;

        cout << "Fibonacci of : " << num << " is: "
            << fiboRecur(num) << endl;
    }
    
    return 0;
}