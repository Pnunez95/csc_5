a/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 20, 2014, 10:44 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
//outputs function oouputs an array of a give size
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
//returns the address of a dynamic array
int* create_dynamic_array(int size)
{
    //creates dynamic array using new keyword
    int* array = new int[size];
    
    for(int i = 0; i < size; i++)
    {
        //array[i] = i; // implicit referencing
        *(array + i) = i;
    }
    return array;
}
//delete to prevent memory leaks
void delete_array(int*p)
{
    delete[] p;
}
int main(int argc, char** argv) {

    int* a = create_dynamic_array(10);
    
    output (a, 10);
    
    delete_array(a);
    return 0;
}

