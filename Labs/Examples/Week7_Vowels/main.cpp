/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 3, 2014, 11:20 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

void append(string &word_1, string word_2);

int main(int argc, char** argv) {

    string word_1;
    string word_2;
    
    cout << "Enter a word." << endl;
    cin >> word_1;
    cout << "Enter a second word." << endl;
    cin >> word_2;
    cout << endl;
    
    append(word_1, word_2);
    return 0;
}

/*
 * 1.Append function adds any vowels located inside the second word to the
 * first word
 * 
 * 2.parameters: two strings representing the first and second word
 * 
 * 3.returns nothing
 */
void append(string &word_1, string word_2)
{
        for(int i = 0; i < word_2.size(); i++)
	{
                if(word_2.substr(i) == "a")
		{
                        word_1 += word_2.substr(i);
		}	
                else if(word_2.substr(i) == "e")
		{
			word_1 += word_2.substr(i);
		}
                else if(word_2.substr(i) == "i")
		{
			word_1 += word_2.substr(i);
		}
                else if(word_2.substr(i) == "o")
		{
			word_1 += word_2.substr(i);
		}
                else if(word_2.substr(i) == "u")
		{
			word_1 += word_2.substr(i);
		}
                else
                {
                    ;
                }
                
	}
        cout << word_1 << endl;
}