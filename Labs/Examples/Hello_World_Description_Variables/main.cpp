/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 20, 2014, 10:33 AM
 */

#include <cstdlib> //csdlib library is not needed
#include <iostream> //iostream is needed for output

// Gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
// There is always and only one main
// Programs always start at main
// Programs execute from top to bottom, left to right
int main(int argc, char** argv) {
    // cout specifies output
    // endl creates a new line
    // << specifeies the stream operator
    // all statements end in a semicolon
    
    // Using a programmer-defined identifer/variable      
    string messege;
    //messege = "Hello World";
    
    //prompt the user
    cout << "Please enter a messege";
    cin >>messege; // user input
    cout << "Your messege is " << messege << endl;
    
    cout << "Hello World" << endl;
    
    // C++ ignores whitespace 
    cout << "Paul Nunez";
    
    // if program hits return, it ran successfully 
    return 0;
} // All code is within curly braces 

