/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 15, 2014, 11:17 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Declare a pointer
    int *p;
    
    // Integer value
    int v = 42;
    
    // Get the address of v
    p = &v;
    
    cout << "The value of p: " << p << endl;
    cout << "Deference p: " << *p << endl;
    cout << "The value of v: " << v << endl;
    
    // Dereference p
    *p = 20;
    cout << endl;
    cout << "Deference p: " << *p << endl;
    cout << "The value of v: " << v << endl;
    
    // Use the new keyword with p
    // New gets a new memory address from the heap
    // stores the value 42 at that address
    p = new int(42);
    cout << endl;
    cout << "The value of p: " << p << endl;
    cout << "Deference p: " << *p << endl;
    cout << "The value of v: " << v << endl;
    
    // Delete p. Deallcoate the memory location
    delete p;
    
    // Dangling pointer error
    // Pointer is pointing to a deallocated memory
    // address. 
    cout << endl;
    cout << "The value of p: " << p << endl;
    cout << "Deference p: " << *p << endl;
    
    // To avoid a dangling pointer error
    // assign the pointer NULL
    //p = NULL;
    //cout << endl;
    //cout << "The value of p: " << p << endl;
    //cout << "Deference p: " << *p << endl;
    
    // For multiple declarations, need to put asterisk
    // on each variable
    int *p1, *p2;
    
    p1 = new int(20);
    
    p2 = p1;
    
    delete p1;
    
    // Error can't do because memory 
    // address already deallocated
    //delete p2;
    
    // Example of a memory leak
    p1 = new int(20);
    cout << "Location: " << p1 << endl;
   
    p1 = new int(42);
    cout << "Lost previous location of p1: " << endl;
    cout << "Location: " << p1 << endl;
    return 0;
}