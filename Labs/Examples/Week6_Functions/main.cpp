/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 27, 2014, 10:45 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

//create/define the square function
//Square function returns the square of a value
//needs 1 parameter, an integer

int square(int num)
{
    return num * num;
}

//define problem 1 function
//problem 1 function runs the square function
void problem1()
{
    cout << "Enter a number." << endl;
    int value;
    cin >> value;
     //invoke the square function
    int squared_value = square(value);
    //using a variable
    cout << "The square of " << value << " is: " << squared_value << endl;
    //outputting the function call
    cout << "The square of " << value << " is: " << square(value)<< endl;
}

//define endprogram function
void endprogram()
{
    cout << "Your program has ended" << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Enter a number." << endl;
    int value;
    cin >> value;

    //invoke the square function
    int squared_value = square(value);
    //using a variable
    cout << "The square of " << value << " is: " << squared_value << endl;
    //outputting the function call
    cout << "The square of " << value << " is: " << square(value)<< endl;
    //using integer literal
    cout << "The square of " << value << " is: " << square(12)<< endl;
    
    //exectue 
    //Menu based operation
    int select;
    do
    {
    cout << "Enter a number: " << endl;
    cout << "1 for square value " << endl;
    cout << "-1 to end" << endl;
    
    int select;
    cin >> select;
    
    switch(select)
    {
        case 1:
            problem1();
            break;
        case -1:
            endprogram();
            break;
    }
    }
    while(select != -1);
    
    return 0;
}


