/*
* Name: Paul Nunez Figueroa
* Student ID: 2486474
* Date: 4.5.14
* Project 1
* I certify this is my own work and code
*/
/* 
 * File:   main.cpp
 * Author: Paul
 * Created on April 5, 2014, 2:43 PM
 */

#include <cstdlib>
#include <iostream>
#include <time.h>
using namespace std;

/*
 * 
 */
int CHIPS = 100;
void blackjack();
void craps();
int main(int argc, char** argv) {

    int select;

    do
    {
        cout << "Project 1: Casino games!" << endl;
        cout << "You have " << CHIPS << " chips." << endl;
        cout << "Press 1 to play Blackjack." << endl;
        cout << "Press 2 to play Craps." << endl;
        cout << "Press 3 to quit program." << endl;
        cin >> select;
        cout << endl;
        
        if(CHIPS == 0)
        {
            cout << "You are out of chips. You lose." << endl;
        }
        
        switch(select)
        {
            case 1:
                blackjack();
                break;
            case 2:
                craps();
                break;
            case 3:
                cout << "Program Ended." << endl;
                break;
        }
    }
    while(select != 3 && CHIPS != 0);
    return 0;
}
/*
 * craps() begins a game of craps. It displays the amount of chips the user 
 * has and accepts a bet from them. Then it generates two numbers between 1 and
 * 6 which are displayed as the two dice numbers and adds them. If the sum is
 * 7 or 11 then the player wins and the bet is added to the player's chips. If 
 * the sum is 2,3, or 11, the player loses and the wager is subtracted from the 
 * player's chips. If the sum isn't any of these numbers, round two begins. 
 * During round 2 the dice are rerolled until the sum is either 7 or the 
 * previous sum. If the sum is 7 the player loses. If it is the previous sum
 * the player wins.
 * 
 * no parameters
 * 
 * no return
 */
void craps()
{
    srand(time(0));
    int bet;
    int dice1 = rand() % (6 - 1 + 1) + 1;
    int dice2 = rand() % (6 - 1 + 1) + 1;    
    int sum = dice1 + dice2;
    
    cout << "Craps!" << endl;
    cout << "You have " << CHIPS << " chips." << endl;
    cout << "Enter wager." << endl;
    cin >> bet;
    cout << endl;
    
    cout << "Dice 1: " << dice1 << endl;
    cout << "Dice 2: " << dice2 << endl;
    cout << "Sum: " << sum << endl;
    cout << endl;
    
    if(sum == 7 || sum == 11)
    {
        cout << "You win!" << endl;
        CHIPS += bet;
    }
    else if(sum == 2 || sum == 3 || sum == 12)
    {
        cout << "You lose." << endl;
        CHIPS -= bet;
    }
    else
    {
        cout << "Sum is " << sum << ". Moving on to round 2." << endl;
        cout << "Round 2." << endl;
        bool round2_end = false;
            
        do
        {
            cout << endl;
            
            int dice3 = rand() % (6 - 1 + 1) + 1;
            int dice4 = rand() % (6 - 1 + 1) + 1;
            int sum2 = dice3 + dice4;
            
            cout << "Dice 1: " << dice3 << endl;
            cout << "Dice 2: " << dice4 << endl;
            cout << "Sum: " << sum2 << endl;
            cout << endl;
            
            if(sum2 == 7)
            {
                cout << "You lose." << endl;
                CHIPS -= bet;
                round2_end = true;
            }
            else if(sum2 == sum)
            {
                cout << "You win." << endl;
                CHIPS += bet;
                round2_end = true;
            }
            else
            {
                cout << "Sum is " << sum2 << ". Rerolling..." << endl; 
            }
        }
        while(round2_end != true);
    }

}

/*
 * blackjack() begins a game of blackjack. Function displays the number of chips
 * the player currently has and accepts a bet from them. Afterwards it deals two
 * cards two the user and if a card is an ace asks them what value they would
 * like for that card. It then asks the player if they would like another card.
 * The function then deals two cards for the dealer and
 * compares the two hands. If the player's hand is larger than the dealer's the
 * player wins.
 * 
 * returns nothing
 * 
 * no parameters
 */
void blackjack()
{
    srand(time(0));
    int wager;
    int player_card_1 = rand() % (13 - 1 + 1) + 1;
    int player_card_2 = rand() % (13 - 1 + 1) + 1;
    int player_hand;
    int dealer_card_1 = rand() % (13 - 1 + 1) + 1;
    int dealer_card_2 = rand() % (13 - 1 + 1) + 1;
    int dealer_hand;
    bool win;
    bool player_21 = false;
    bool player_bust = false;
    bool tie;
    bool cont = false;
    
    cout << "Blackjack!" << endl;
    cout << "You have " << CHIPS << " chips" << endl;
    cout << "Enter your wager." << endl;
    cin >> wager;
    cout << endl;
    cout << "Your cards are: " << endl;
    //Player Card 1
    cout << "Card 1: ";
    if(player_card_1 == 11)
    {
        cout << "Jack" << endl;
        player_card_1 = 10; 
    }
    else if(player_card_1 == 12)
    {
        cout << "Queen" << endl;
        player_card_1 = 10;
    }
    else if(player_card_1 == 13)
    {
        cout << "King" << endl;
        player_card_1 = 10;
    }
    else if(player_card_1 == 1)
    {
        cout << "Ace." << endl;
    }
    else
    {
        cout << player_card_1 << endl;
    }
    
    //Player Card 2
    cout << "Card 2: ";
    if(player_card_2 == 11)
    {
        cout << "Jack" << endl;
        player_card_2 = 10; 
    }
    else if(player_card_2 == 12)
    {
        cout << "Queen" << endl;
        player_card_2 = 10;
    }
    else if(player_card_2 == 13)
    {
        cout << "King" << endl;
        player_card_2 = 10;
    }
    else if(player_card_2 == 1)
    {
        cout << "Ace" << endl;
    }
    else
    {
        cout << player_card_2 << endl;
    }
    //Possible Aces
    if(player_card_1 == 1)
    {
        int a1;
        cout << "Card 1 is an ace. Would you like a 1 or 11?" << endl;
        cin >> a1;
        if(a1 == 1)
        {
            player_card_1 = 1;
            cout << "Card 1 is now 1." << endl;
        }
        else
        {
            player_card_1 = 11;
            cout << "Card 1 is now 11." << endl;
        }
    }
    if(player_card_2 == 1)
    {
        int a2;
        cout << "Card 2 is an ace. Would you like a 1 or 11?" << endl;
        cin >> a2;
        if(a2 == 1)
        {
            player_card_2 = 1;
            cout << "Card 2 is now 1." << endl;
        }
        else
        {
            player_card_2 = 11;
            cout << "Card 2 is now 11." << endl;
        }
    }
    //Player hit or stay
    int s1;
    player_hand = player_card_1 + player_card_2;
    
    cout << "Your hand is " << player_hand << endl;
    if(player_hand == 21)
    {
        cout << "You win!" << endl;
        win = true;
        player_21 = true;
    }
    else
    {
         do
        {    
        cout << "Press 1 to hit. Press 2 to stay." << endl;
        cin >> s1;   
        switch(s1)
        {
            case 1:
                int new_card;
                new_card = rand() % (10 - 1 + 1) + 1;

                if(new_card == 1)
                {
                    int a3;
                    cout << "New card is an ace. Would you like a 1 or 11?" 
                            << endl;
                    cin >> a3;
                    if(a3 == 1)
                    {
                        new_card = 1;
                        player_hand += new_card;
                        if(player_hand > 21)
                        {
                            cout << "Hand is now: " << player_hand << endl;
                            cout << "Bust. You lose." << endl;
                            win = false;
                            player_bust = true;
                            s1 = 2;
                        }
                        else if(player_hand == 21)
                        {
                            cout << "21! You win!" << endl;
                            win = true;
                            player_21 = true;
                            s1 =2;
                        }
                        else
                        {
                            cout << "Hand is now: " << player_hand << endl;
                         }
                    }
                    else
                    {
                        new_card = 11;
                        player_hand += new_card;
                        if(player_hand > 21)
                        {
                            cout << "Hand is now: " << player_hand << endl;
                            cout << "Bust. You lose." << endl;
                            win = false;
                            player_bust = true;
                            s1 = 2;
                        }
                        else if(player_hand == 21)
                        {
                            cout << "21! You win!" << endl;
                            win = true;
                            player_21 = true;
                            s1 = 2;
                        }
                        else
                        {
                            cout << "Hand is now: " << player_hand << endl;
                        }

                    }
                }
                else
                {
                    player_hand += new_card;
                    if(player_hand > 21)
                    {
                        cout << "Hand is now: " << player_hand << endl;
                        cout << "Bust. You lose." << endl;
                        win = false;
                        player_bust = true;
                        s1 = 2;
                    }
                    else if(player_hand == 21)
                    {
                         cout << "21! You win!" << endl;
                         win = true;
                         player_21 = true;
                         s1 = 2;
                    }
                    else
                    {
                         cout << "Hand is now: " << player_hand << endl;
                    }
                }

                break;
            case 2:
                    cout << "Stay on " << player_hand << endl;
                    bool cont = true;
                break;
        }
    }
    while(s1 !=2);       
    }
    //if(player_21 == true || player_bust == true)
    cout << endl;
    cout << "Dealer's card's are: " << endl;
    //Dealer card 1
    cout << "Card 1: ";
    if(dealer_card_1 == 11)
    {
        cout << "Jack" << endl;
        dealer_card_1 = 10; 
    }
    else if(dealer_card_1 == 12)
    {
        cout << "Queen" << endl;
        dealer_card_1 = 10;
    }
    else if(dealer_card_1 == 13)
    {
        cout << "King" << endl;
        dealer_card_1 = 10;
    }
    else if(dealer_card_1 == 1)
    {
        cout << "Ace." << endl;
        if( 11 + dealer_card_2 > 17)
        {
            dealer_card_1 = 1;
            cout << "Card 1 is now 1." << endl;
        }
        else
        {
            dealer_card_1 = 11;
            cout << "Card 1 is now 11." << endl;
        }
        
    }
    else
    {
        cout << dealer_card_1 << endl;
    }
    
    //Dealer Card 2
    cout << "Card 2: ";
    if(dealer_card_2 == 11)
    {
        cout << "Jack" << endl;
        dealer_card_2 = 10; 
    }
    else if(dealer_card_2 == 12)
    {
        cout << "Queen" << endl;
        dealer_card_2 = 10;
    }
    else if(dealer_card_2 == 13)
    {
        cout << "King" << endl;
        dealer_card_2 = 10;
    }
    else if(dealer_card_2 == 1)
    {
        cout << "Ace." << endl;
        if( 11 + dealer_card_1 > 17)
        {
            dealer_card_2 = 1;
            cout << "Card 2 is now 1." << endl;
        }
        else
        {
            dealer_card_2 = 11;
            cout << "Card 2 is now 11." << endl;
        }
        
    }
    else
    {
        cout << dealer_card_2 << endl;
    }
    
    //Dealer's hand
    dealer_hand = dealer_card_1 + dealer_card_2;
    cout << "Dealer's hand is now: " << dealer_hand << endl;
    
    if(dealer_hand >= 17)
    {
        cout << "Dealer stays with " << dealer_hand << endl;
        if(dealer_hand < player_hand && player_hand < 21)
        {
            cout << "You win with " << player_hand << endl;
            win = true;
        }
        else if(player_hand < dealer_hand && dealer_hand < 21)
        {
            win = false;
        }
        else if(dealer_hand == player_hand)
        {
            cout << "Tie game." << endl;
            tie = true;
        }
        else
        {
            cout << "You lose. Dealer wins with " << dealer_hand << endl;
            win = false;
        }
    }
    else if (dealer_hand == 21)
    {
        cout << "You lose. Dealer wins with 21." << endl;
        win = false;
    }
    else
    {
        do
        {
            int dealer_new_card;
            dealer_new_card = rand() % (10 - 1 + 1) + 1;
            dealer_hand += dealer_new_card; 
            if(dealer_hand < player_hand && player_hand < 21)
            {
                cout << "You win with " << player_hand << endl;
                win = true;
            }
            else if(player_hand < dealer_hand && dealer_hand < 21)
            {
                win = false;
            }            
            else if(dealer_hand > 21)
            {
                cout << "You win. Dealer is bust with " << dealer_hand << endl;
                win = true;
            }
            else if(dealer_hand == 21)
            {
                cout << "You lose. Dealer wins with 21." << endl;
                win = false;
            }

            else if(player_hand == dealer_hand)
            {
                tie = true;
            }
            else
            {
                cout << "Dealer's hand is now: " << dealer_hand << endl;
            }
            
        }
        while(dealer_hand <= 16);
        
        if(tie == true)
        {
            cout << "Tie game. No chips earned or lost." << endl;
        }
        
        if(win == true)
        {
            cout << "You win with " << player_hand << endl;
            CHIPS += wager;
            cout << "You now have " << CHIPS << " chips" << endl;
        }
        else
        {
            cout << "Dealer wins with " << dealer_hand << endl;
            CHIPS -= wager;
            cout << "You now have " << CHIPS << " chips" << endl;
        }
        cout << endl;
    }
    }
 


