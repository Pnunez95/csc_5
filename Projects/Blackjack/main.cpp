/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on April 5, 2014, 3:06 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(0));
    int wager;
    int player_card_1 = rand() % (13 - 1 + 1) + 1;
    int player_card_2 = rand() % (13 - 1 + 1) + 1;
    int player_hand;
    int dealer_card_1 = rand() % (13 - 1 + 1) + 1;
    int dealer_card_2 = rand() % (13 - 1 + 1) + 1;
    int dealer_hand;
    bool win;
    
    cout << "Blackjack!" << endl;
    cout << "Your cards are: " << endl;
    //Player Card 1
    cout << "Card 1: ";
    if(player_card_1 == 11)
    {
        cout << "Jack" << endl;
        player_card_1 = 10; 
    }
    else if(player_card_1 == 12)
    {
        cout << "Queen" << endl;
        player_card_1 = 10;
    }
    else if(player_card_1 == 13)
    {
        cout << "King" << endl;
        player_card_1 = 10;
    }
    else if(player_card_1 == 1)
    {
        int a1;
        cout << "Ace. Would you like a 1 or 11?" << endl;
        cin >> a1;
        if(a1 == 1)
        {
            player_card_1 = 1;
            cout << "Card 1 is now 1." << endl;
        }
        else
        {
            player_card_1 = 11;
            cout << "Card 1 is now 11." << endl;
        }
        
    }
    else
    {
        cout << player_card_1 << endl;
    }
    
    //Player Card 2
    cout << "Card 2: ";
    if(player_card_2 == 11)
    {
        cout << "Jack" << endl;
        player_card_2 = 10; 
    }
    else if(player_card_2 == 12)
    {
        cout << "Queen" << endl;
        player_card_2 = 10;
    }
    else if(player_card_2 == 13)
    {
        cout << "King" << endl;
        player_card_2 = 10;
    }
    else if(player_card_2 == 1)
    {
        int a2;
        cout << "Ace. Would you like a 1 or 11?" << endl;
        cin >> a2;
        if(a2 == 1)
        {
            player_card_2 = 1;
            cout << "Card 2 is now 1." << endl;
        }
        else
        {
            player_card_2 = 11;
            cout << "Card 2 is now 11." << endl;
        }
        
    }
    else
    {
        cout << player_card_2 << endl;
    }
    
    //Player hit or stay
    int s1;
    player_hand = player_card_1 + player_card_2;
    
    cout << "Your hand is " << player_hand << endl;
    if(player_hand == 21)
    {
        cout << "You win!" << endl;
        win = true;
    }
    else
    {
         do
        {    
        cout << "Press 1 to hit. Press 2 to stay." << endl;
        cin >> s1;   
        switch(s1)
        {
            case 1:
                int new_card;
                new_card = rand() % (10 - 1 + 1) + 1;

                if(new_card == 1)
                {
                    int a3;
                    cout << "New card is an ace. Would you like a 1 or 11?" 
                            << endl;
                    cin >> a3;
                    if(a3 == 1)
                    {
                        new_card = 1;
                        player_hand += new_card;
                        if(player_hand > 21)
                        {
                            cout << "Hand is now: " << player_hand << endl;
                            cout << "Bust. You lose." << endl;
                            win = false;
                            s1 = 2;
                        }
                        else if(player_hand == 21)
                        {
                            cout << "You win!" << endl;
                            win = true;
                            s1 =2;
                        }
                        else
                        {
                            cout << "Hand is now: " << player_hand << endl;
                         }
                    }
                    else
                    {
                        new_card = 11;
                        player_hand += new_card;
                        if(player_hand > 21)
                        {
                            cout << "Hand is now: " << player_hand << endl;
                            cout << "Bust. You lose." << endl;
                            win = false;
                            s1 = 2;
                        }
                        else if(player_hand == 21)
                        {
                            cout << "You win!" << endl;
                            win = true;
                            s1 = 2;
                        }
                        else
                        {
                            cout << "Hand is now: " << player_hand << endl;
                        }

                    }
                }
                else
                {
                    player_hand += new_card;
                    if(player_hand > 21)
                    {
                        cout << "Hand is now: " << player_hand << endl;
                        cout << "Bust. You lose." << endl;
                        win = false;
                        s1 = 2;
                    }
                    else if(player_hand == 21)
                    {
                         cout << "You win!" << endl;
                         win = true;
                         s1 = 2;
                    }
                    else
                    {
                         cout << "Hand is now: " << player_hand << endl;
                    }
                }

                break;
            case 2:
                    cout << "Stay on " << player_hand << endl;            
                break;
        }
    }
    while(s1 !=2);       
    }
    
    cout << "Dealer's card's are: " << endl;
    //Dealer card 1
    cout << "Card 1: ";
    if(dealer_card_1 == 11)
    {
        cout << "Jack" << endl;
        dealer_card_1 = 10; 
    }
    else if(dealer_card_1 == 12)
    {
        cout << "Queen" << endl;
        dealer_card_1 = 10;
    }
    else if(dealer_card_1 == 13)
    {
        cout << "King" << endl;
        dealer_card_1 = 10;
    }
    else if(dealer_card_1 == 1)
    {
        cout << "Ace." << endl;
        if( 11 + dealer_card_2 > 17)
        {
            dealer_card_1 = 1;
            cout << "Card 1 is now 1." << endl;
        }
        else
        {
            dealer_card_1 = 11;
            cout << "Card 1 is now 11." << endl;
        }
        
    }
    else
    {
        cout << dealer_card_1 << endl;
    }
    
    //Dealer Card 2
    cout << "Card 2: ";
    if(dealer_card_2 == 11)
    {
        cout << "Jack" << endl;
        dealer_card_2 = 10; 
    }
    else if(dealer_card_2 == 12)
    {
        cout << "Queen" << endl;
        dealer_card_2 = 10;
    }
    else if(dealer_card_2 == 13)
    {
        cout << "King" << endl;
        dealer_card_2 = 10;
    }
    else if(dealer_card_2 == 1)
    {
        cout << "Ace." << endl;
        if( 11 + dealer_card_1 > 17)
        {
            dealer_card_2 = 1;
            cout << "Card 2 is now 1." << endl;
        }
        else
        {
            dealer_card_2 = 11;
            cout << "Card 2 is now 11." << endl;
        }
        
    }
    else
    {
        cout << dealer_card_2 << endl;
    }
    
    //Dealer's hand
    dealer_hand = dealer_card_1 + dealer_card_2;
    cout << "Dealer's hand is now: " << dealer_hand << endl;
    
    if(dealer_hand >= 17)
    {
        cout << "Dealer stays with " << dealer_hand << endl;
        if(dealer_hand < player_hand)
        {
            cout << "You win with " << player_hand << endl;
            win = true;
        }
        else if(dealer_hand == player_hand)
        {
            cout << "Tie game." << endl;
            win = false;
        }
        else
        {
            cout << "You lose. Dealer wins with " << dealer_hand << endl;
            win = false;
        }
    }
    else if (dealer_hand == 21)
    {
        cout << "You lose. Dealer wins with 21." << endl;
    }
    else
    {
        do
        {
            int dealer_new_card;
            dealer_new_card = rand() % (10 - 1 + 1) + 1;
            dealer_hand += dealer_new_card; 
            if(dealer_hand > 21)
            {
                cout << "You win. Dealer is bust with " << dealer_hand << endl;
                win = true;
            }
            else if(dealer_hand == 21)
            {
                cout << "You lose. Dealer wins with 21." << endl;
                win = false;
            }
            else if(dealer_hand == 17)
            {
                cout << "Dealer's hand is now 17. Dealer stays. " << endl;
            }
            else if(dealer_hand > 17 && dealer_hand != 21)
            {
                if(dealer_hand > player_hand)
                {
                    win = false;
                }
            }
            else
            {
                cout << "Dealer's hand is now: " << dealer_hand << endl;
            }
            
        }
        while(dealer_hand <= 16);
        
        if(win == true)
        {
            cout << "You win with " << player_hand << endl;
        }
        else
        {
            cout << "Dealer wins with " << dealer_hand << endl;
        }
    }
    
    return 0;
}

