/* 
 * File:   main.cpp
 * Author: Paul
 *
 * Created on April 12, 2014, 4:14 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
//void craps_round2();
int main(int argc, char** argv) {

    srand(time(0));
    int bet;
    int dice1 = rand() % (6 - 1 + 1) + 1;
    int dice2 = rand() % (6 - 1 + 1) + 1;    
    int sum = dice1 + dice2;
    
    cout << "Craps!" << endl;
    cout << "Enter wager." << endl;
    cin >> bet;
    cout << endl;
    
    cout << "Dice 1: " << dice1 << endl;
    cout << "Dice 2: " << dice2 << endl;
    cout << "Sum : " << sum << endl;
    cout << endl;
    
    if(sum == 7 || sum == 11)
    {
        cout << "You win!" << endl;
    }
    else if(sum == 2 || sum == 3 || sum == 12)
    {
        cout << "You lose." << endl;
    }
    else
    {
        cout << "Sum is " << sum << ". Moving on to round 2." << endl;
        cout << "Round 2." << endl;
        bool round2_end = false;
            
        do
        {
            cout << endl;
            
            int dice3 = rand() % (6 - 1 + 1) + 1;
            int dice4 = rand() % (6 - 1 + 1) + 1;
            int sum2 = dice3 + dice4;
            
            cout << "Dice 1: " << dice3 << endl;
            cout << "Dice 2: " << dice4 << endl;
            cout << "Sum : " << sum2 << endl;
            cout << endl;
            
            if(sum2 == 7)
            {
                cout << "You lose." << endl;
                round2_end = true;
            }
            else if(sum2 == sum)
            {
                cout << "You win." << endl;
                round2_end = true;
            }
            else
            {
                cout << "Sum is " << sum2 << ". Rerolling..." << endl; 
            }
        }
        while(round2_end != true);
    }

    return 0;
}

