Paul Nunez
2486474
4.12.14
CSC-5-43621

The menu interface works fine except for one thing. Even though the program ends when the player runs out of chips, 
the message "You are out of chips. You lose" is not displayed.

The blackjack game has a couple bugs I could not fix. Not always, but sometimes the wager, whether the player wins or loses, is not
added/subtracted to the total chips. I also couldn't figure out how to end the game if the player gets a 21, the
game continues even if the player does get 21 or goes over 21. I attempted to put an if statement that said
if(player_21 == true || player_bust == true) with everything below inside of it but it didn't work. The program would
end when it should have continued, and continued when it should have ended. The other bug I found had to do with a tie game.
If the game resulted in a tie, sometimes the player would still end up losing money.

Craps works perfectly. No bugs were found. Strangely, the wager system I used in the blackjack game, which often did not work,
works well in the craps game.

