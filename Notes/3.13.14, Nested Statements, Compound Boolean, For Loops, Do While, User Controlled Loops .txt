NESTED STATEMENTS

	int x = 0;
	if(x == 0)
	{
		if(x < 10)
		{
			x++;
		}
		else
		{
			x--;
		}
	}
	else
	{
		x--;
	}

Determine if # is positive or negative, and even or odd

	//prompt the user
	int num;
	cout << "Enter a number";
	cin >> num;
	if(num > 0)
	{
		cout >> "Positive";
			if(num%2 == 0) // even
			{
				cout << "Even";
			}
			Else
			{
				cout << "Odd";
			}
	}
	else
	{
		cout >> "Negative";
			if(num%2 == 0) // even
			{
				cout << "Even";
			}
			Else
			{
				cout << "Odd";
			}
	}

COMPOUND BOOLEAN

-&& - and, || - or
-&& requires both left and right condition to be true for the entire statement to be true

	int x = 0;
	if(x == 0 && x < 10)
	{
		x++;
	}
	else if (x == 0 && x > 10)
	{
		x--;
	}
	else
	{
		x--;
	}

LOOPS

1. INITIALIZATION
2. CONDITION
3. INCREMENTOR

FOR LOOP

Syntax:
	for(initialization; condition; incrementor)
	{
		//code
	}

-For loops have known conditions
	ex. 20 loops

	//Loop(iterate) 10 times
		1	2	4
	for(int i = 0; i < 10; i++)
	{
	3	cout << i << endl;
	}

	i	output
	0	0
	1	1
	2	2
	3	3
	4	4
	5	5
	6	6
	7	7
	8	8
	9	9
	10 <- False

DO WHILE

-Execute code at least once
-Syntax: 
	do
	{
		//code
	}
	while(condition);
	{
		//code
	}
	
	Example
	
	int x = 0;
	do
	{
		x++;
	}
	while(x < 0);
		//false
	
USER CONTROLLED LOOPS

-Typically done with do-while, or while 

	//prompt the user
	cout << "Enter a number"
	     << "-1 to quit";
	//user enters -1 or quit to stop
	int num;
	cin >> num;
	while(num != -1)
	{
		cout << "Entered " << num;
	     	//pompt
		cout << "Enter another number";	
	     	     << "-1 to quit";
		cin >> num;
	}