SIMPLE CONTROL FLOW

Tuesday
-if statement
	ex. int x = 0;
	if x (x < 10) <- can only be evaluated as true/false(bool)
	{
	x++
	}
		
	Either or 
	int x = 3;
	if (x < 0)
	{
	x++
	}
	else
	{
	x--
	}

IF ELSE IF BLOCK
Syntax : 
	int x = 0;
	if x (x < 0)
	{
	x++
	}
	else if (x > 0)
	{
	x--
	}
	else
	{
	x = 20;
	}
-Else if has conditions
-Else if always has an else
-Each if-block must have 1 satisfied condition
-Each if statement begins a new block
	
	int x = 5;
	if ( x > 10 )
	{
	x++
	}
	else if ( x > 9 )
	{
	x--
	} 
	else if ( x > 6 )
	{
	x--
	}
	else 
	{
	x == 25
	}

	int x = 5;
	if ( x > 10 )
	{
	x++
	}
	else if ( x < 7 ) <-- true executed & program stops checking conditions
	{
	x--
	}
	else if ( x < 6 )
	{	
	x--
	}

SWITCH CASE

Syntax:
	switch(variable_to_switch)
	{
	case val_of_var_1:
	//code break;(only break allowed)
	case val_of_var_2:
	//code break;
	case val_of_var_3:
	//code break;

-Switch cases used for menu driven program
-Variable can only be an int or char
-Default acts like else. Is executed if no other cases are satisfied

	int num;
	cout << "Enter a number between 1-5";
	cin >> num;
	switch(num)
	{
	case 1:
	cout << "Entered 1";
	break;
	case 2:
	cout << "Entered 2"; 
	break;
	default:
	cout << "Invalid";
	break;
	}

WHILE LOOPS

-Execute code multiple times
-3 components
	1.Initialization (where to start)
	2.Condition (when to stop)
	3.Incrementor / value change
	3.5. Code

-syntax:
	Initialization
	while(condition)
	{
	//code		//order does not matter
	incrementor;	//(for now)
	}

-Output value between 0 and 10

	start 0, end 10

	int x = 0;(initialization)
	while( x <= 10 )
loop	{ (condition)
back to	cout << x << endl;
condit	x++; (incrementor)
        }

	x	Output

	0	0
	1	1
	2	2
	3	3
	4	4
	5	5
	6	6
	7	7
	8	8
	9	9
	10	10
-Most common error is being one off

COMPOUND BOOLEAN STATEMENTS

x < y < z?

-&& - and operator (shit 7, twice)
-|| - or operator (below backspace key)
-Allows conditions to be combined

	if ( x < y && y < z )
		//both conditions have to be true in order to have the compound be true
	x = 6;
	y = 8;
	z = 10;
	
	//x < y	y < z
	// true && true -> true
	
	x = 8;
	x = 6;
	z = 10;

	//x < y y < z
	//false && true -> false

	x < y || y < z (|| either condition needs to be true)
	8 < 6	 y < 10
	false || true -> true
	