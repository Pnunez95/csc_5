STYLE RULES

-code should not be longer than eighty characters per line
-output should not be longer than eighty characters per line(line-wrap)
-good variable names
-all code tabbed and indented equally

HEIRARCHY OF DATA TYPES

-variables and literals can only interact with same data type
	ex. 1 + 3 (both ints.)
		int x = 2.9
		(double to int implicit conversion)
		(2.9 becomes 2)
		(2 is assigned to x)
*largest
	double
	float(floating point decimal)
	int
*smallest
	ex. double -> int
	"demotion"
	ex. int -> float
	"promotion"

-coersion:act of  promotion or demotion

OUTPUT FORMATTING

-only changes output
-<iomanip> library
	keywords:
		set w() : defines or sets the width of the next output(only works once)
		setprecision() : defines either the number of decimals or the number of numbers
		fixed : forces setprecision() to use number of decimals
		right : forces output to the right side (had to be manipulated by setw())
		left : forces output to the left side 
		setfill() : fills in all blacks of set w()  
			() = any character

EXAMPLES

//number w/ 2 decimal places
cout << setprecision(2) << fixed;
cout << 12.3259;
//output shows 12.33;
cout << setprecision(3);
//output is 12.325
cout << "12.3259";
//output is 12.3259
cout << setw(10) << left;
cout << 12.3259;
//setw() only works once
cout << 12.3259;

cout << setw(10) << left << setfill(+)
<<12.3 << 12.3259;
12.3+++++++12.3259

cout << setw(10) << right <<setfill(+)
<< 12.3 << 12.358;
+++++++12.312.358

CONSTANTS

-use keywords "const"
	ex. const double PI = 3.14
-use all caps for const variables
-use on numbers that never change

ERRORS



