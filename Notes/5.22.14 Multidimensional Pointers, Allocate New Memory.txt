MULTIDIMENSIONAL POINTERS

3 FUNCTIONS
Create Multidimensional Array
Fill Value
Output

int **create_multidimensional_array(int row, int col)
{
	//create pointer to pointer array
	int **p = new int*[row];
	//iterate through all rows
	for(int i < 0; i < row; i++)
	{
		p[i] = new int[col];
	}
	return p;
}

void fill_value(int **p, int row, int col) //need size
{
	//use nested for loops
	//row
	for(int i = 0; i < row; i++)
	{
		//col
		for(int j = 0; j < col; j++)
		{
			p[i][j]= i * col + j + 1;
		}
	}
}

void output(int **p, int row, int col)
{
	//row
	for(int i = 0; i < row; i++)
	{
		//col
		for(int j = 0; j < col; j++)
			cout << p[i][j] << " ";
			cout << endl;
	}
}

ALLOCATE NEW MEMORY
void allocate_memory(int *&p, int &size)
{

	int temp_size = size * 2;
	//1.create new array
	int *tempPtr = new int[temp_size
	//2.copy value
	for(int i = 0; i < size; i++)
		tempPtr[i] = p[i];
	//3.delete orginal array to avoid memory leaks
	delete[] p;
	size = temp_size;
	//4.point to new array
	p = temp;
}

STRUCTURES AND CLASSES
Syntax:
struct BankAccount{
	int acc;
	string name;
	double balance;
	};

int main()
{
BankAccount acc1;
//dot operator
acc1.balance = 1000;
acc1.name = "Matt";
acc1.acc = 123456;

void output(BankAccount acc)
{
cout << acc.balance << endl;
}
