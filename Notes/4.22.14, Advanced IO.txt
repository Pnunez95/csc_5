ADVANCED IO

cin buffer 
|H|e|l|l|o||W|o|r|l|d|

	string word;
	cin >> word; Hello
	
>> - stream insertion operator, reads until whitespace or newline and then ignores the character

getline(buffer, string);
-void function, needs buffer and a string
-reads until a new line is found and ignores it

Ex.
	string word;
	getline(cin, word);
	//word will extract "Hello World"

cin.get(c); only gets a single character from buffer
c = cin.get();